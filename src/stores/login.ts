import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";
export const useloginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const LoginName = ref("");
  const isLogin = computed(() => {
    return LoginName.value !== "";
  });
  const login = (userName: string, Password: string): void => {
    if (userStore.login(userName, Password)) {
      LoginName.value = userName;
      localStorage.setItem("LoginName", userName);
    } else {
      messageStore.showMessage("Login or Password is incorrect");
    }
  };
  const Logout = (): void => {
    LoginName.value = "";
    localStorage.removeItem("LoginName");
  };
  const loadData = (): void => {
    LoginName.value = localStorage.getItem("LoginName") || "";
  };

  return { LoginName, isLogin, login, Logout, loadData };
});
